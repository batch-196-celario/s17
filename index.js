/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInfo(){
		let fullName = prompt('Enter your Full name');
		let Age = prompt('Enter your Age');
		let Location = prompt('Enter your location');

		console.log('Hii ' + fullName);
		console.log(fullName + "s age is " + Age);
		console.log(fullName + " is located at " + Location); 
	};

	userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favorites(){
		let favoriteBandAndMusicians = ['Coldplay', 'khalid', 'Maroon 5', 'Because', 'Arthur Nery'];
		console.log(favoriteBandAndMusicians);
	};
	favorites();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function FaveMovies(){
		console.log('1. Jujutsu Kaisen 0');
		console.log('Tomatometer for Jujutsu Kaisen 0: 98%');
		console.log('2. Attack on titan');
		console.log('IMDB Rating: 9/10');
		console.log('3. From');
		console.log('Tomatometer for From: 94%');
		console.log('4. Overlord');
		console.log('IMDB Rating: 7.7/10');
		console.log('5. Extreme Job');
		console.log('Tomatometer for Extreme Job: 82%');
	};
	FaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);